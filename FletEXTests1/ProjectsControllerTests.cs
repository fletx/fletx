﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FletEX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FletEX.Models;

namespace FletEX.Tests
{
    [TestClass()]
    public class ProjectsControllerTests
    {
        [TestMethod()]
        public void DetailsTestRedirect()
        {
            var controller = new ProjectsController();
            var result = (RedirectToRouteResult) controller.Details(-1);
            Assert.AreEqual(result.RouteValues["action"], "Index");
        }
    }
}