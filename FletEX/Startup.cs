﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FletEX.Startup))]
namespace FletEX
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
