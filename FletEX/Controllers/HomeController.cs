﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FletEX.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult EmailSend()
        {
            ViewBag.Message = "Send E-mail Page";

            return View();
        }
        public ActionResult ProcessRequest()
        {
            ViewBag.Message = "Process Request";
            return View();
        }

    }

}