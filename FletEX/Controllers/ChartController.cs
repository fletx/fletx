﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FletEX.Models;

namespace FletEX.Controllers
{
    public class ChartController : Controller
    {
        private FletXEntities db = new FletXEntities();
        // GET: Chart
        public ActionResult Index()
        {
            return View(db.Projects.ToList());
        }

        // GET: Chart/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Chart/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Chart/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Chart/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Chart/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Chart/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Chart/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public PartialViewResult Chart()
        {
            return PartialView();
        }

        public ActionResult YearProjectChart(int pid)
        {
            var myChart = new System.Web.Helpers.Chart(width: 600, height: 400);

            myChart.SetYAxis("", 2000, 2100);
            myChart.AddTitle("Chart Title");

            List<string> list = new List<string>();
            List<string> list2 = new List<string>();
            foreach (var item in db.Projects)
            {
                list.Add(item.Name);
                list2.Add(item.DateStart.Year.ToString());

            }
            myChart.AddSeries(name: "Projetcs", xValue: list.ToArray(), yValues: list2.ToArray());

            myChart.GetBytes("jpeg");

            var mChart = myChart.GetBytes("jpeg");

            return File(mChart, "image/jpeg");
        }

    }
}
