﻿using FletEX.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FletEX
{
    public class TasksController : Controller
    {
        private FletXEntities db = new FletXEntities();

        // GET: Tasks
        public ActionResult Index()
        {
            var tasks = db.Tasks.Include(t => t.Projects);
            return View(tasks.ToList());
        }

        // GET: Tasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Tasks tasks = db.Tasks.Find(id);
            if (tasks == null)
            {
                return HttpNotFound();
            }
            ViewData["Comments"] = db.Comments.Where(t => t.Tasks_Id == id);
            return View(tasks);
        }

        // GET: Tasks/Create
        public ActionResult Create(int? id)
        {
            //ViewBag.Project_Id = new SelectList(db.Projects, "Id", "Name");
            ViewBag.Project_Id = id;
            return View();
        }

        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Titles,Steps,Types,Substeps,Priorities,Project_Id,DateStart,DateEnd")] Tasks tasks)
        {
            if (ModelState.IsValid) 
            {
                db.Tasks.Add(tasks);
                db.SaveChanges();
                return RedirectToAction("../Projects/Details/" + tasks.Project_Id);
            }

            ViewBag.Project_Id = new SelectList(db.Projects, "Id", "Name", tasks.Project_Id);
            return View(tasks);
        }

        // GET: Tasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tasks tasks = db.Tasks.Find(id);
            if (tasks == null)
            {
                return HttpNotFound();
            }
            ViewBag.Project_Id = new SelectList(db.Projects, "Id", "Name", tasks.Project_Id);
            return View(tasks);
        }

        // POST: Tasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Titles,Steps,Types,Substeps,Priorities,Project_Id,DateStart,DateEnd")] Tasks tasks)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tasks).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("../Projects/Details/" + tasks.Project_Id);
            }
            ViewBag.Project_Id = new SelectList(db.Projects, "Id", "Name", tasks.Project_Id);
            return View(tasks);
        }

        // GET: Tasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tasks tasks = db.Tasks.Find(id);
            if (tasks == null)
            {
                return HttpNotFound();
            }
            return View(tasks);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tasks tasks = db.Tasks.Find(id);
            db.Tasks.Remove(tasks);
            db.SaveChanges();
            return RedirectToAction("../Projects/Details/" + tasks.Project_Id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
